import * as ActionTypes from "./ActionTypes";

export const Employees = (
    state = {
        isLoadingEmployees: true,
        errMess: null,
        employees: []
    },
    action) => {
    switch (action.type) {

        case ActionTypes.ADD_EMPLOYEE:
            return {...state, isLoadingEmployees: false, errMess: null, employees: action.payload};

        case ActionTypes.EMPLOYEES_LOADING:
            return {...state, isLoadingEmployees: true, errMess: null, employees: []};
        case ActionTypes.EMPLOYEE_FAILED:
            return {...state, isLoadingEmployees: false, errMess: action.payload, employees: []};

        default:
            return state;
    }
};
