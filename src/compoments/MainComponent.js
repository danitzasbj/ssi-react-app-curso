import React, {Component} from 'react';
import CatalogComponent from "./CatalogComponent";
import Header from "./HeaderComponent"
import Footer from "./FooterComponent"
import {Redirect, Route, Switch, withRouter} from 'react-router-dom'
import Home from "./HomeComponent";
import Contact from "./ContactComponent"
import About from "./AboutComponent"
import ItemdetailComponent from "./ItemdetailComponent";
import {connect} from "react-redux";
import {addComment, fetchComments, fetchEmployees, fetchItems, postComment} from "../redux/ActionCreators";
import {actions} from "react-redux-form";

const mapStateToProps = (state) => {
    return {
        items: state.items,
        employees: state.employees,
        comments: state.comments
    }
};

const mapDispatchToProps = dispatch => ({
    addComment: (itemId, rating, author, comment) => dispatch(addComment(itemId, rating, author, comment)),
    fetchItems: () => dispatch(fetchItems()),
    resetFeedbackForm: () => {
        dispatch(actions.reset('feedback'))
    },
    fetchComments: () => dispatch(fetchComments()),
    postComment: (itemId, rating, author, comment) => dispatch(postComment(itemId, rating, author, comment)),

    fetchEmployees: () => dispatch(fetchEmployees()),
    resetFeedbackForm: () => {
        dispatch(actions.reset('feedback'))
    },
});

class Main extends Component {


    constructor(props, context) {
        super(props, context);
        console.log("Main constructor es invocado");
    }

    componentDidMount() {
        this.props.fetchItems();
        this.props.fetchComments();
        this.props.fetchEmployees();
    }
    render() {
        const HomePage = () => {
            return (
                <Home
                    item={this.props.items.items.filter(item => item.featured)[0]}
                    employee={this.props.employees.employees.filter(employee => employee.featured)[0]}
                    itemsLoading={this.props.items.isLoading}
                    itemsErrMess={this.props.items.errMess}
                />
            );
        };

        const ItemWithId = ({match}) => {
            return (
                <ItemdetailComponent
                    item={this.props.items.items.filter((item) => item.id === parseInt(match.params.itemId, 10))[0]}
                    itemsLoading={this.props.items.isLoading}
                    itemsErrMess={this.props.items.errMess}
                    comments={this.props.comments.comments.filter((comment) => comment.itemId === parseInt(match.params.itemId, 10))}
                    commentsErrMess={this.props.comments.errMess}
                    addComment={this.props.addComment}
                    postComment = {this.props.postComment}
                />

            );
        };

        return (
            <div>
                <Header/>
                <Switch>
                    <Route path='/home' component={HomePage}/>
                    <Route exact path='/catalog'
                           component={() => <CatalogComponent items={this.props.items}/>}/>

                    <Route path='/catalog/:itemId' component={ItemWithId}/>

                      <Route path='/contactus'
                           component={() => <Contact resetFeedbackForm={this.props.resetFeedbackForm}/>}/>


                    <Route exact path='/aboutus'
                           component={() => <About employees={this.props.employees}/>}/>

                    <Redirect to="/home"/>
                </Switch>
                <Footer/>

            </div>
        );
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));
