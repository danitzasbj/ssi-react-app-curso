import React, {Component} from 'react';
import './App.css';
import Main from "./compoments/MainComponent";
import {ITEMS} from "./shared/items";
import {BrowserRouter} from "react-router-dom";
import {ConfigureStore} from "./redux/configureStore";
import {Provider} from "react-redux";
import {EMPLOYEES} from "./shared/employees";

const store = ConfigureStore();

class App extends Component {


    constructor(props, context) {
        super(props, context);
        this.state = {
            items: ITEMS,
            employees: EMPLOYEES
        }
    }


    render() {
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <div className="App">
                        <Main/>
                    </div>
                </BrowserRouter>
            </Provider>
        );
    }

}

export default App;
